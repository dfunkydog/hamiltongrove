<?php
/**
* The template for displaying lookbook collection
 * Template Name: lookbooks
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<article>

	<h2><?php the_title(); ?></h2>
	<?php the_content(); ?>			

	<!--Child Page Thumbnails Start-->
	<div id="lookbook-thumbs">
	<?php $subs = new WP_Query( array( 'post_parent' => $post->ID, 'post_type' => 'page', 'meta_key' => '_thumbnail_id', 'orderby' => 'menu_order' ));
	    if( $subs->have_posts() ) : while( $subs->have_posts() ) : $subs->the_post();

	    echo '<div class="lookbook-perm"><a href="' . get_permalink( $thumbnail->ID ) . '" title="' . esc_attr( $thumbnail->post_title ) . '">';
	      echo get_the_post_thumbnail($thumbnail->ID, 'medium');
	      echo '</a>';
	      echo '<p>'.get_the_title($ID).'</p></div>';



	    endwhile; endif; wp_reset_postdata(); ?>
	    </div>
	<!--Child Page Thumbnails End-->

</article>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
