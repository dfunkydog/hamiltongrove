jQuery(document).ready(function ($) {
	var $bricks = $('.page-template-the-front-page-php article');
    
	$bricks.imagesLoaded(function(){
		$bricks.masonry({
            itemSelector: '.wp-caption',
            // set columnWidth a fraction of the container width
            columnWidth: function (containerWidth) {
                return containerWidth / 2;
            }
        })	
	})

    $(".sub-menu").hide();
    $("li").hover(function() {
        $(this).find(".sub-menu").show();
        console.log($(this));
    }, function() {
        $(this).find(".sub-menu").hide();
    });  
});


